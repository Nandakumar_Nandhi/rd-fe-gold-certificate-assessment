import { Component ,Input} from '@angular/core';
import { Router } from '@angular/router';
import { Book } from 'src/Book';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent {
@Input() book!:Book;
  constructor(private router:Router){}
  editForm(){
    this.router.navigate(['/edit',this.book.id])
  }
}
