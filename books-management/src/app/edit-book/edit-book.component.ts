import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.css']
})
export class EditBookComponent {
  constructor(private fb:FormBuilder){}
  editForm:FormGroup=this.fb.group(
    {
      title:['',Validators.required],
      author:['',Validators.required],
      price:[,Validators.required]
    }
  )
  editBook(){

  }
}
