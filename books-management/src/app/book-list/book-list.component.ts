import { Component } from '@angular/core';
import { BookServiceService } from '../book-service.service';
import { Book } from 'src/Book';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent {
  constructor(private bookService:BookServiceService){}
  books!:Book[];
  ngOnInit():void{
    this.books=this.bookService.getBooks();
  }
}
