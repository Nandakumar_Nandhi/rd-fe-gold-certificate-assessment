import { Injectable } from '@angular/core';
import { Book } from 'src/Book';

@Injectable({
  providedIn: 'root'
})
export class BookServiceService {

  constructor() { }
  books=[
    {
      id:'2',
      title:"Java",
      author:"hyrufr",
      price:90
    },
    {
      id:'3',
      title:"Js",
      author:"hyrufr",
      price:90
    },
    {
      id:'4',
      title:"html",
      author:"hyrufr",
      price:90
    }
  ]
  getBooks():Book[]{
    return this.books;
  }
  createBook(newBook:Book){
    this.books.push(newBook);
  }
}
