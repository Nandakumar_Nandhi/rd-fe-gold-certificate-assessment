import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BookServiceService } from '../book-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.css']
})
export class CreateBookComponent {
  constructor(private fb:FormBuilder,private bookService:BookServiceService,private router:Router){}
  createForm:FormGroup=this.fb.group(
    {
      title:['',Validators.required],
      author:['',Validators.required],
      price:[,Validators.required]
    }
  )
  createBook(){
    let book={
      id:Date.now()+((Math.random()*100000).toFixed()),
      title:this.createForm.controls['title'].value,
      author:this.createForm.controls['author'].value,
      price:this.createForm.controls['price'].value,
    }
    this.bookService.createBook(book);
    this.router.navigate(['/']);
  }
  get title(){
    return this.createForm.controls['title'];
  }
}
